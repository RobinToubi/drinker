package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"drinker/pkg/common"
	"drinker/pkg/handlers"
	"drinker/pkg/routes"

	"github.com/gorilla/mux"
)

func main() {
	DB := common.Init()
	h := handlers.New(DB)
	r := mux.NewRouter()
	routes.UserRouter = r.PathPrefix("/users").Subrouter()
	routes.TransactionRouter = r.PathPrefix("/transactions").Subrouter()
	setupRouter(&h)

	fmt.Println("Launched at localhost:" + os.Getenv("PORT"))
	port := ":" + os.Getenv("PORT")
	log.Fatal(http.ListenAndServe(port, r))
}

func setupRouter(h *handlers.Handler) {
	routes.InitUserRoutes(&h.User)
	routes.InitTransactionRoutes(&h.Transaction)
}
