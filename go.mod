module drinker

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v4 v4.16.1 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	golang.org/x/crypto v0.0.0-20220511200225-c6db032c6c88
	gorm.io/driver/postgres v1.3.5
	gorm.io/gorm v1.23.5
)
