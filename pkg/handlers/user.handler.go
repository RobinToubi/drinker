package handlers

import (
	"drinker/pkg/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

func (h *UserHandler) GetAllUsers(w http.ResponseWriter, r *http.Request) {
	var users *[]models.User
	if result := h.DB.Find(&users); result.Error != nil {
		fmt.Println(result.Error)
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&users)
}

func (h *UserHandler) GetUserById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	var user *models.User
	if result := h.DB.First(&user, id); result.Error != nil {
		fmt.Println(result.Error)
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(user)
}

func (h *UserHandler) Create(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
	}
	var user models.User
	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Println(err)
		return
	}
	hashedPassword, err := hashPassword(user.Password)
	if err != nil {
		log.Println(err)
		return
	}
	user.Password = hashedPassword
	if result := h.DB.Create(&user); result.Error != nil {
		log.Println(result.Error)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(user)
}

func hashPassword(password []byte) ([]byte, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword(password, 12)
	if err != nil {
		log.Fatalln("Error on hashing password : %w", err)
		return nil, err
	}
	return hashedPassword, nil
}
