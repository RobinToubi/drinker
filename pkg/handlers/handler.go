package handlers

import (
	"gorm.io/gorm"
)

type Handler struct {
	User        UserHandler
	Transaction TransactionHandler
}

func New(db *gorm.DB) Handler {
	return Handler{
		User:        UserHandler{db},
		Transaction: TransactionHandler{db},
	}
}

type UserHandler struct {
	DB *gorm.DB
}

type TransactionHandler struct {
	DB *gorm.DB
}
