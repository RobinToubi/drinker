package handlers

import (
	"drinker/pkg/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

func (th *TransactionHandler) Search(w http.ResponseWriter, r *http.Request) {
	var transactions *[]models.Transaction
	queryParams := r.URL.Query()
	var fields []string
	var values []interface{}
	if label := queryParams.Get("label"); label != "" {
		fields = append(fields, "label LIKE ?")
		values = append(values, "%"+label+"%")
	}
	if debitor := queryParams.Get("debitorId"); debitor != "" {
		if debitorId, err := strconv.ParseUint(debitor, 10, 64); err == nil {
			fields = append(fields, "debitor_id = ?")
			values = append(values, uint(debitorId))
		}
	}
	if creditor := queryParams.Get("creditorId"); creditor != "" {
		if creditorId, err := strconv.ParseUint(creditor, 10, 64); err == nil {
			fields = append(fields, "creditor_id = ?")
			values = append(values, uint(creditorId))
		}
	}
	if state := queryParams.Get("state"); state != "" {
		if stateId, err := strconv.Atoi(state); err == nil {
			fields = append(fields, "state = ?")
			values = append(values, stateId)
		}
	}
	results := th.DB.Debug().Where(strings.Join(fields, " AND "), values...).Find(&transactions)

	w.Header().Add("Content-Type", "application/json")
	if results.Error != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	err := json.NewEncoder(w).Encode(transactions)
	if err != nil {
		return
	}
}

func (th *TransactionHandler) Create(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Error when trying to get body")
		return
	}
	var transaction *models.Transaction
	if err = json.Unmarshal(body, &transaction); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	transaction.State = models.Pending
	if result := th.DB.Create(&transaction); result.Error != nil {
		fmt.Println(result.Error)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(&transaction)
}

func (th *TransactionHandler) ById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	var transaction *models.Transaction
	w.Header().Add("Content-Type", "application/json")
	if result := th.DB.First(&transaction, id); result.Error != nil {
		fmt.Println(result.Error)
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&transaction)
}

func (th *TransactionHandler) Delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	if result := th.DB.Delete(&id); result.Error != nil {
		log.Printf("Error when trying to delete transaction with id : %d", id)
		log.Println(result.Error)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode("ok")
}
