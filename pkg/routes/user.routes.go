package routes

import (
	"drinker/pkg/handlers"
	"net/http"

	"github.com/gorilla/mux"
)

var UserRouter *mux.Router

func InitUserRoutes(h *handlers.UserHandler) {
	UserRouter.HandleFunc("/{id:[0-9]+}", h.GetUserById).Methods(http.MethodGet)
	UserRouter.HandleFunc("", h.GetAllUsers).Methods(http.MethodGet)
	UserRouter.HandleFunc("", h.Create).Methods(http.MethodPost)
	UserRouter.HandleFunc("", h.Create).Methods(http.MethodPost)
}
