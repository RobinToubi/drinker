package routes

import (
	"drinker/pkg/handlers"
	"net/http"

	"github.com/gorilla/mux"
)

var TransactionRouter *mux.Router

func InitTransactionRoutes(th *handlers.TransactionHandler) {
	TransactionRouter.HandleFunc("", th.Search).Methods(http.MethodGet)
	TransactionRouter.HandleFunc("/{id:[0-9]+}", th.ById).Methods(http.MethodGet)
	TransactionRouter.HandleFunc("", th.Create).Methods(http.MethodPost)
	TransactionRouter.HandleFunc("/{id:[0-9]+}", th.Delete).Methods(http.MethodDelete)
}
