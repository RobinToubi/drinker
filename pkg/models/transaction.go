package models

import "gorm.io/gorm"

type Transaction struct {
	gorm.Model
	CreditorID uint   `gorm:"primaryKey"`
	DebitorID  uint   `gorm:"primaryKey"`
	State      int    `gorm:"enum"`
	Label      string `gorm:"size:100" json:"label"`
}

const (
	Refused  = 0
	Pending  = 1
	Accepted = 2
)
