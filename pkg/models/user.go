package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	PhoneNumber string        `gorm:"primaryKey;unique" json:"phone"`
	Nickname    string        `gorm:"not null;check:nickname<>''" json:"nickname"`
	Password    []byte        `json:"-"`
	Credits     []Transaction `gorm:"foreignKey:DebitorID" json:"credits"`
	Debits      []Transaction `gorm:"foreignKey:CreditorID" json:"debits"`
}
