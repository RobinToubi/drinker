package common

import (
	"drinker/pkg/models"
	"fmt"
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Init() *gorm.DB {
	env := os.Getenv("DB_DRINKER")
	db, err := gorm.Open(postgres.Open(env), &gorm.Config{})
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("DB connection successful")

	err = db.AutoMigrate(&models.Transaction{})
	err = db.AutoMigrate(&models.User{})
	if err != nil {
		log.Fatalln("Erreur when migrating")
	}

	return db
}
